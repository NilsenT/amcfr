function [Ensemble_reconstruction_inst]=Analogue_ensemble(nclosest,Rec_ensemblesize)

%Ensemble version of the analogue method
% by Dr. Tine Nilsen 22.02.2021.
% See method description in Nilsen et al. 2021 (manuscript in preparation, full citation will be added after publication.)

%Data import

cd('datapath');
load('Truetemp.mat')                                                    %Load  target data (truth).
truth_X=Data;                                                           %Dimensions: nlocs x nyears.
Proxy_locs=Masterlocs(proxyindex,:);                                    %Locations with proxy information.
Instr_locs=Masterlocs(instindex,:);                                     %Locations with instrumental information (all locs).


load('SNR_10.mat');                                                     %Load (pseudo)-proxy and (pseudo)-instrumental data with selected SNR.
%load('SNR_1.mat');
%load('SNR_05.mat');
pproxy_X=proxy(proxyindex,:);
Inst_X=inst(:,1001:1156);   


% Each proxy is associated with an ensemble of possible chronologies. 
%Import the chronology ensemble (Dimensions: nyears x nchronologies). 

% Run the analogue methodology repeatedly, each time selecting a
% different chronology for each proxy. Save the reconstruction ensemble.
PSR_ensemble_Rec05=zeros(size(truth_X,1),size(truth_X,2),Rec_ensemblesize);


for p=1:Rec_ensemblesize                                                %Ensemble member p must be less or equal to the total number of available chronologies.
    nproxies=length(proxyindex);                                        %Number of proxy records within the reconstruction region.
for i=1:nproxies
    cd('~/OneDrive - UiT Office 365/PALEOBRIDGE/Paper 1/Codes_and_data_public/SNR_chron');

    formatSpec = 'SNR05_agemodel_%d.mat';
        str = sprintf(formatSpec,i);
    load(str);                                                          %Import chronology ensemble for proxy(i).

random_chron=chron(:,randi(1000));                                      %Randomly select a chronology for proxy nr i. 

uniquevals=unique(random_chron);                                        %Remove double-counted years from the chronology.                                    

time=linspace(850,2005,1156);                                           %True age.

indtab=zeros(1,length(time));                                           %Index table.

for n=1:length(uniquevals)                  
if ismember(uniquevals(n),time)
indtab(n)=uniquevals(n);                                                %Retain the chronology only within the reconstruction time period.                                  
end
end

 missing=find(~ismember(time,indtab));                                  %Identify missing years from the chronology.
 present=find(ismember(time,indtab));
 newdata=zeros(1,1156);
 newdata(present)=data(present);                                        %Retain data values associated with the current chronology.
 newdata(missing)=nan;
 
proxy(proxyindex(i),:)=newdata;
end




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%% RECONSTRUCTION USING INSTRUMENTAL DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Reconstruction_inst=zeros(size(truth_X,1),size(truth_X,2));

y0 = size(pproxy_X,2)-size(Inst_X,2);                               %y0==1000 years before start of inst data.

 %The entire pool of analogues comprise every time slice in the
 %instrumental period.
                                                                    %number of closest analogues selected for averaging. 
        

%Calculate the Euclidian distance between the proxy data at time t and
%every time slice in the model pool. Order the distances and select the
%nclosest analogues, the SST field reconstruction at time t is the average
%of the SST field in the nclosest analogues.

Distance_p=zeros(length(proxyindex),size(Inst_X,2));   
Distance=zeros(1,size(Inst_X,2));


for t=1:size(pproxy_X,2)

    for inst_years=1:size(Inst_X,2)
        for i=1:size(pproxy_X,1)
        Distance_p(i,inst_years) = (pproxy_X(i,inst_years+y0) - pproxy_X(i,t) ).^2 ;
        end
        Distance(inst_years)=sqrt(sum(Distance_p(:,inst_years)));
    end
    [values, index]=sort(Distance);
    Reconstruction_inst(:,t)=mean(Inst_X(:,index(1:nclosest)),2);    %% Average over the npool closest analogues

end


for i=1:size(proxyindex,2)
    ii=proxyindex(i);
   Reconstruction_inst(ii,:)= pproxy_X(i,:);                        %Replace the reconstruction time series with the proxy data  at proxy locations.
end

Ensemble_reconstruction_inst(:,:,p)=Reconstruction_inst;
end

%Optional: save the reconstruction ensemble to file.
%cd('yourpath');
%save('Ensemble_PSR_Rec.mat', 'Ensemble_reconstruction_inst');   