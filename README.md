# README #

This README document describes the MATLAB code for the analogue method.
The code is described in detail and used for pseudo-proxy reconstruction in Nilsen et al. (2021). See also Talento et al. (2019) where similar code is applied in a pseudo-proxy framework.
The documented version of the analogue method is denoted as "classic", in which the analogue pool is formed by time slices from the instrumental period. A real-world reconstruction generated with this method will be independent from climate model simulations. The code is tested for field reconstruction of sea surface temperature over the North Atlantic realm in Nilsen et al. (2021). 
The reconstruction skill is tested for annual and decadal temporal resolution over the past millennium.

### What is this repository for? ###

Open source MATLAB code for the analogue methodology, used for climate field reconstruction. The repository also includes the annually resolved data sets used as input to the reconstruction method in Nilsen et al. (2021).

### How do I get set up? ###

Download the code directly, or checkout from this git repository in the command window: git clone https://NilsenT@bitbucket.org/NilsenT/amcfr.git
### Contribution guidelines ###
Please cite this code if used in published work. You can create a pull request for me (Tine Nilsen) if you want to contribute to the code.

### Who do I talk to? ###

Dr. Tine Nilsen, Department of Mathematics and Statistics, UiT the Arctic University of Norway, Tromsø, Norway. Drop me an email / message, either through the BitBucket interface or tine.nilsen@uit.no

References:
Nilsen. T., S. Talento and J. P. Werner (2021), Constraining two climate field reconstruction methodologies over the North Atlantic realm using pseudo-proxy experiments, Quaternary Science Reviews (accepted).

Talento. S, L. Schneider, J. P. Werner and J. Luterbacher (2019), Millennium-length precipitation reconstruction over south-eastern Asia: a pseudo-proxy approach. Earth System Dynamics, 10, 347–364, https://doi.org/10.5194/esd-10-347-2019.